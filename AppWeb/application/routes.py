from application import application
from flask import render_template, request, redirect, url_for, make_response, jsonify
from werkzeug.utils import secure_filename
import os
import cv2
from PIL import Image
import numpy as np
import operator
from torchvision import models, transforms
import torch
from torch import nn

ALLOWED_EXTENSIONS = set(['jpeg', 'JPEG', 'JPG', 'PNG', 'bmp'])

loadModel = os.path.dirname (__file__)


categories_list = ['0', '1', '2', '3', '4']

device = torch.device('cpu')
model = models.resnet50(pretrained=False)
model.fc = torch.nn.Sequential(nn.Linear(2048, 1024),
                               nn.ReLU(),
                               nn.Linear(1024, 256),
                               nn.ReLU(),
                               nn.Dropout(0.2),
                               nn.Linear(256, 5),
                               nn.LogSoftmax(dim=1))

weights = torch.load(loadModel+'/mask_classifier_ResNet50.pt', map_location=device)
model.load_state_dict(weights)
model.to(device)
model.eval()

image_transforms = transforms.Compose([transforms.Resize(224),
                                       transforms.CenterCrop((224, 224)),
                                       transforms.ToTensor(),
                                       transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])])


def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

@application.route('/')
def index():
    return render_template('index.html')

@application.route('/index')
def loadImg():
    return render_template('')

@ application.route('/upload', methods = ['POST', 'GET']) # agregar una ruta
def upload():
    print("1")
    if request.method == 'POST':
        f = request.files['file']
        if not (f and allowed_file(f.filename)):
            return render_template('fail.html')

        user_input = f.filename
        basepath = os.path.dirname (__file__) # ruta del archivo actual
        upload_path = os.path.join (basepath, 'static/images', secure_filename (f.filename))

        f.save(upload_path)
        img = cv2.imread(upload_path)
        cv2.imwrite(os.path.join(basepath, 'static/images', 'test.jpg'), img)

        c,p,mp = classify_image()

        return render_template('Imagen_cargada.html',userinput=user_input, category=c, probability=p, model_probabilities=mp)
    print("12")
    return render_template('fail.html')

def classify_image():
    basepath = os.path.dirname (__file__)
    print("aaaaaaaaaa_"+basepath)
    image = cv2.imread(basepath + '/static/images/test.jpg')
    image = Image.fromarray(np.uint8(image))
    tensor = image_transforms(image)
    tensor.unsqueeze_(0)

    model_result = model.forward(tensor.to(device))
    model_probabilities = torch.exp(model_result).tolist()[0]
    index, probability = max(enumerate(model_probabilities), key=operator.itemgetter(1))
    category = categories_list[index]

    return round(int(category),2), round(probability,2), model_probabilities

