from flask import Flask, render_template

application = Flask(__name__)

from application import routes