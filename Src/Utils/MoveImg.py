import pandas as pd
import os
import shutil
import random
from PIL import Image
import albumentations as A
import cv2
import imageio
import matplotlib.pyplot as plt
'''Autor: ALlan Veloza Rodriguez
   Fecha: 16/Sep/2021
   Descripcion: Esta clase realiza la clasificacion de imagenes por carpetas.
   Ajusta las categorias para evitar el desbalanceo'''


class MoveImg(object):

    def __init__(self):
        self.df_train_labels = None
        self.pathFile = "C:\\Users\\ALLAN CHUPAPIJAS\\Documents\\RNA\\train1\\trainLabels\\trainLabels.csv"
        self.pathFinal = "C:\\Users\\ALLAN CHUPAPIJAS\\Documents\\RNA\\train1\\"
        #self.pathOrigin = "C:\\Users\\ALLAN CHUPAPIJAS\\Documents\\RNA\\train1\\train\\"
        self.pathTrain = "C:\\Users\\ALLAN CHUPAPIJAS\\Documents\\RNA\\train1\\train\\"
        self.pathVal = "C:\\Users\\ALLAN CHUPAPIJAS\\Documents\\RNA\\train1\\validation\\"
        self.pathTest = "C:\\Users\\ALLAN CHUPAPIJAS\\Documents\\RNA\\train1\\test\\"
        self.unbalancedCount = None

    def create_classes(self):
        try:
            for i in range(5):
                os.mkdir(self.pathFinal + str(i))
        except:
            print("Las Clases ya estan creadas")

    def read_csv_classes(self):
        try:
            self.df_train_labels = pd.read_csv(self.pathFile)
            print(self.df_train_labels)
        except:
            print("No se pudo leer el archivo")

    def transform_dataframe(self):
        for data in self.df_train_labels:
            self.df_train_labels[['number', 'side']] = self.df_train_labels['image'].str.split("_", expand=True)
            self.df_train_labels['level_str'] = self.df_train_labels['level'].astype(str)
            self.df_train_labels['image_complete'] = self.df_train_labels['image'] + ".jpeg"
            self.df_train_labels['label'] = self.df_train_labels['side'] + "_" + self.df_train_labels['level_str']

    def move_classes(self):
        for indice in self.df_train_labels.index:
            shutil.move(self.pathOrigin + self.df_train_labels['image_complete'][indice],
                        self.pathFinal + self.df_train_labels['level_str'][indice])
        

    def unbalanced_class(self):
        longitudes_list = []
        for i in range(5):
            lista = os.listdir(self.pathFinal + str(i))
            longitudes_list.append(len(lista))
            longitudes_list.sort()
        self.unbalancedCount = longitudes_list[0]
        print(longitudes_list)
        print("La clase mas desbalanceada tiene: {" + str(self.unbalancedCount) + "} Imagenes")


    def leveler_classes(self):
        '''
            en promedio de las clases 1,2,3 y 4 es de 2329
            (708 + 873 + 2443 + 5292)/4 = 2329
            asi que se toma este valor para balancear las otras clases.
        '''
        self.unbalancedCount = 2329
        try:
            for i in range(5):
                list_img = os.listdir(self.pathFinal + str(i))
                print("Numero de elementos iniciales en clase { " + str(i) + " } : " + str(len(list_img)))
                list_sample = random.sample(list_img, self.unbalancedCount)
                for j in list_sample:
                    list_img.remove(j)
                print("Numero de elementos a excluir en clase { " + str(i) + " } : " + str(len(list_img)))
                print("Inica nivelando clase { " + str(i) + " } ")
                for r in list_img:
                    os.remove(self.pathFinal + str(i) + "\\" + r)
                print("El conteo en la categoria { " + str(i) + "} es de: " + str(len(os.listdir(self.pathFinal + str(i)))) + " imagenes.")
        except ValueError:
            print("Clases balanceadas." + ValueError)
    
    def resize_img(self):
        basewidth = 500
        for i in range(5):
            list_img = os.listdir(self.pathFinal + str(i))
            for j in list_img:
                img = Image.open(self.pathFinal+"/"+str(i)+"/"+j)
                wpercent = (basewidth / float(img.size[0]))
                hsize = int((float(img.size[1]) * float(wpercent)))
                img = img.resize((basewidth, hsize), Image.ANTIALIAS)
                img.save(self.pathFinal+"/"+str(i)+"/r"+j)
                os.remove(self.pathFinal+"/"+str(i)+"/"+j)
                
    def augmentation(self):
        try:
            list_img = os.listdir(self.pathFinal + "4")
            print("Numero de elementos iniciales en clase { 4 } : " + str(len(list_img)))
            for r in (list_img):                
                input_img = imageio.imread(self.pathFinal + "4\\" + r)
                transform = A.Compose([
                A.RandomRotate90(),
                A.Transpose(),
                A.ShiftScaleRotate(shift_limit=0.08, scale_limit=0.5, rotate_limit=45, p=.8),
                A.Blur(blur_limit=7),
                A.GridDistortion(),
                ])
                
                random.seed(2)
                basepath = os.path.dirname (__file__)
                augmented_image = transform(image=input_img)['image']
                Image.fromarray(augmented_image).save(self.pathFinal + "4\\a3_" + r)
            print("El conteo en la categoria { 4 } es de: " + str(len(os.listdir(self.pathFinal + "4"))) + " imagenes.")
        except ValueError:
            print("Clases balanceadas." + ValueError)
            
    
    def split_data(self):
        '''
            Total: 2329
            70% entrenamiento: 1631
            15% validacion: 349
            15% pruebas: 349
        '''
        cantidadDividir = 349
        try:
            for i in range(5):
                list_img = os.listdir(self.pathFinal + str(i))
                print("Numero de elementos iniciales en clase { " + str(i) + " } : " + str(len(list_img)))
                list_sample = random.sample(list_img, cantidadDividir)
                for j in list_sample:
                    list_img.remove(j)
                print("Numero de elementos a excluir en clase { " + str(i) + " } : " + str(len(list_img)))
                print("Inica nivelando clase { " + str(i) + " } ")
                for r in list_img:
                    shutil.move(self.pathFinal + str(i) + "\\" + r, self.pathTest + str(i) + "\\" + r)
        except ValueError:
            print("Clases divididas." + ValueError)
            

# %%
if __name__ == "__main__":
    implImg = MoveImg()
    ##implImg.create_classes()
    ##implImg.read_csv_classes()
    ##implImg.transform_dataframe()
    ##implImg.move_classes()
    implImg.unbalanced_class()
    ##implImg.leveler_classes()
    ##implImg.augmentation()
    ##implImg.resize_img()
    implImg.split_data()
    
