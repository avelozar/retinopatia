from torchvision import models, transforms
import torch
from torch import nn
from PIL import Image
import operator
import cv2
import numpy as np
import pandas as pd
from os import walk


# arreglar paths
weights_path = 'C:/Users/ALLAN CHUPAPIJAS/Documents/RNA/retinopatia-rna/Src/Rna/mask_classifier_ResNet50.pt'
validation_path = 'C:/Users/ALLAN CHUPAPIJAS/Documents/RNA/train2/val'

categories_list = ['0', '1', '2', '3', '4']

device = torch.device('cpu')
model = models.inception_v3(pretrained=False)
model.fc = torch.nn.Sequential(nn.Linear(2048, 1024),
                               nn.ReLU(),
                               nn.Linear(1024, 256),
                               nn.ReLU(),
                               nn.Dropout(0.2),
                               nn.Linear(256, 5),
                               nn.LogSoftmax(dim=1))

weights = torch.load(weights_path, map_location=device)
model.load_state_dict(weights)
model.to(device)
model.eval()


image_transforms = transforms.Compose([transforms.Resize(224),
                                       #transforms.CenterCrop((224, 224)),
                                       transforms.ToTensor(),
                                       transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])])

validation_files = list()
validation_real_category = list()

for (_, dir_names, _) in walk(validation_path):
    for directory in dir_names:
        for (_, _, file_names) in walk(f'{validation_path}/{directory}'):
            for file_name in file_names:
                validation_files.append(validation_path + "/" + directory + "/" + file_name)
                validation_real_category.append(directory)
    break

category_list = list()
probability_list = list()

for file in validation_files:
   #a = validation_path + "/" + directory + "/" + file
    numpy_image = cv2.imread(file)
    image = Image.fromarray(np.uint8(numpy_image))
    tensor = image_transforms(image)
    tensor.unsqueeze_(0)

    model_result = model.forward(tensor.to(device))
    model_probabilities = torch.exp(model_result).tolist()[0]
    index, probability = max(enumerate(model_probabilities), key=operator.itemgetter(1))
    category = categories_list[index]

    category_list.append(category)
    probability_list.append(probability)

results = pd.DataFrame(list(zip(validation_files, validation_real_category, category_list, probability_list)),
                       columns=['file', 'real', 'resnet_category', 'resnet_probability'])

results.to_csv('validation_result2.csv')